/*
	The MIT License (MIT)

	Copyright (c) 2015 Pham Quy

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

#import "UIScrollView+PIParallax.h"
#import <objc/runtime.h>
#define SYNTHESIZE_CATEGORY_OBJ_PROPERTY(propertyGetter, propertySetter)                                                             \
- (id) propertyGetter {                                                                                                             \
return objc_getAssociatedObject(self, @selector( propertyGetter ));                                                             \
}                                                                                                                                   \
- (void) propertySetter (id)obj{                                                                                                    \
objc_setAssociatedObject(self, @selector( propertyGetter ), obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);                            \
}


#define SYNTHESIZE_CATEGORY_VALUE_PROPERTY(valueType, propertyGetter, propertySetter)                                                \
- (valueType) propertyGetter {                                                                                                      \
valueType ret = {0};                                                                                                                  \
[objc_getAssociatedObject(self, @selector( propertyGetter )) getValue:&ret];                                                    \
return ret;                                                                                                                     \
}                                                                                                                                   \
- (void) propertySetter (valueType)value{                                                                                           \
NSValue *valueObj = [NSValue valueWithBytes:&value objCType:@encode(valueType)];                                                \
objc_setAssociatedObject(self, @selector( propertyGetter ), valueObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);                       \
}


typedef BOOL PIParallaxScrollDirection;
#define PIParallaxScrollDirectionUp     YES
#define PIParallaxScrollDirectionDown   NO



/**
 *  This is private class, and it is not supposed to be use independently
 */
@interface _PIParallaxController : NSObject
@property(nonatomic,strong) UIView * parallaxContainer;
@property(nonatomic,assign) CGFloat windowHeight;
@property(nonatomic,assign) CGFloat parallaxFactor;
@property(nonatomic,assign) BOOL  snappy;
@property(nonatomic) BOOL enable;
@property(nonatomic,weak) UIScrollView* scrollView;
@property(nonatomic) UIEdgeInsets orginalInset;
- (instancetype) initWithScrollView:(UIScrollView*) scrollView;
@end

@implementation _PIParallaxController
- (instancetype) initWithScrollView:(UIScrollView*) scrollView
{
    self = [super init];
    if (self) {
        self.scrollView = scrollView;
    }
    return self;
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{

    CGPoint new = [[change objectForKey:NSKeyValueChangeNewKey] CGPointValue];
    CGPoint old = [[change objectForKey:NSKeyValueChangeOldKey] CGPointValue];

    CGFloat movement = new.y - old.y;

    if (fabs(movement) < FLT_EPSILON) {
        return;
    }
    //NSLog(@"%f", new.y);
    [self contentOffsetUpdate:(new.y + _scrollView.contentInset.top) movement:movement];
}

//------------------------------------------------------------------------------
-(void)contentOffsetUpdate:(CGFloat)offsetY
                  movement:(CGFloat) movement
{
    //    NSLog(@"Offset %f ; movement %f", offsetY, movement);
    CGRect newParallaxFrame = CGRectZero;
    CGFloat top = 0.f;
    CGFloat diff = fabs(self.parallaxContainer.frame.size.height - self.windowHeight) <
    DBL_EPSILON ? 0.f : fabs(self.parallaxContainer.frame.size.height - self.windowHeight);
    
    if (offsetY > 0)
    {
        CGFloat topBound = MIN(self.windowHeight, offsetY);
        top =  -_scrollView.contentInset.top + topBound * self.parallaxFactor - diff/2.;
    }else{
        top = -self.parallaxContainer.frame.size.height - self.orginalInset.top;
    }
    

    newParallaxFrame = CGRectMake(0,
                                  top,
                                  self.parallaxContainer.frame.size.width,
                                  self.parallaxContainer.frame.size.height);
    
    //    NSLog(@"Updated frame: %@", NSStringFromCGRect(newParallaxFrame));
    self.parallaxContainer.frame = newParallaxFrame;
    //    self.parallaxContainer.hidden = YES;
    [_scrollView sendSubviewToBack:self.parallaxContainer];
    
}

- (void) setParallaxContainer:(UIView *)parallaxContainer
{
    _parallaxContainer = parallaxContainer;
    if (_orginalInset.top) {
        _parallaxContainer.frame = CGRectMake(0,
                                          _parallaxContainer.frame.origin.y - _orginalInset.top,
                                          _parallaxContainer.frame.size.width,
                                          _parallaxContainer.frame.size.height);
    }
}
//------------------------------------------------------------------------------
- (void) setScrollView:(UIScrollView *)scrollView
{
    _scrollView = scrollView;
    _orginalInset = scrollView.contentInset;
}
//------------------------------------------------------------------------------
- (void) setEnable:(BOOL)enable
{
    if (enable != self.enable) {
        _enable = enable;
        if (enable ) {
            _scrollView.contentInset = UIEdgeInsetsMake(self.windowHeight + _orginalInset.top, 0, 0, 0);
            [_scrollView addSubview:self.parallaxContainer];
            [_scrollView sendSubviewToBack:self.parallaxContainer];
            [_scrollView addObserver:self
                   forKeyPath:@"contentOffset"
                      options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                      context:NULL];
        }else{
            [_scrollView removeObserver:self forKeyPath:@"contentOffset"];
            _scrollView.contentInset = _orginalInset;
            [self.parallaxContainer removeFromSuperview];
        }
    }
}

- (void) dealloc
{
    [self setEnable:NO];
}
@end

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

@interface UIScrollView ()
@property (nonatomic, strong) _PIParallaxController* pikiScrollviewParallaxController;
@end

@implementation UIScrollView (PIParallax)
SYNTHESIZE_CATEGORY_OBJ_PROPERTY(pikiScrollviewParallaxController, setPikiScrollviewParallaxController:)


- (void) addParallaxBackView:(UIView*) backgroundView
{
    [self addParallaxBackView:backgroundView
                         withHeight:backgroundView.frame.size.height
                     parallaxFactor:0.5];

}

- (void) addParallaxBackView:(UIView*) backgroundView
                        withHeight:(CGFloat) height
{
    [self addParallaxBackView:backgroundView withHeight:height parallaxFactor:0.5];
}


//------------------------------------------------------------------------------
- (void) addParallaxBackView:(UIView*) backgroundView
                        withHeight:(CGFloat) windowHeight
                    parallaxFactor:(CGFloat) factor
{
    
    CGFloat deltaH = fabs(windowHeight - backgroundView.frame.size.height);
    backgroundView.frame = CGRectMake(0,
                                      -windowHeight - deltaH/2.,
                                      backgroundView.frame.size.width,
                                      backgroundView.frame.size.height);

    
    _PIParallaxController* controller = [[_PIParallaxController alloc] initWithScrollView:self];
    controller.parallaxContainer = backgroundView;
    controller.parallaxFactor = factor;
    controller.windowHeight = windowHeight;
    
    controller.enable = YES;
    [self registerController:controller];
}

//------------------------------------------------------------------------------
- (void) removeParallaxView
{
    if (self.pikiScrollviewParallaxController) {
        self.pikiScrollviewParallaxController.enable = NO;
        [self setPikiScrollviewParallaxController:nil];
    }
}
//------------------------------------------------------------------------------
- (void)registerController:(_PIParallaxController*) controller
{
    if (self.pikiScrollviewParallaxController) {
        self.pikiScrollviewParallaxController.enable = NO;
        [self setPikiScrollviewParallaxController:nil];
    }
     [self setPikiScrollviewParallaxController:controller];
}

@end
