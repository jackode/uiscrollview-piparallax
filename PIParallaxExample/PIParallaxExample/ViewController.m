//
//  ViewController.m
//  PIParallaxExample
//
//  Created by Pham Quy on 2/5/15.
//  Copyright (c) 2015 Jkorp. All rights reserved.
//

#import "ViewController.h"
#import "UIScrollView+PIParallax.h"

@interface ViewController () <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *scrollview;
@property (nonatomic, strong) UIView* parallaxView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _parallaxView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 300)];
    UIImageView* iview= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"download.png"]];
    [iview setFrame: _parallaxView.bounds];
    
    UIButton* but = [UIButton buttonWithType:(UIButtonTypeContactAdd)];
    but.center = CGPointMake(100, 100);
    
    [_parallaxView addSubview:iview];
    [_parallaxView addSubview:but];
    [_parallaxView setBackgroundColor:[UIColor grayColor]];
    [_scrollview addParallaxBackView:_parallaxView];
    [_scrollview setBackgroundColor:[UIColor whiteColor]];
    [_scrollview setRowHeight:100];
    [_scrollview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellId"];
    _scrollview.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [_scrollview removeParallaxView];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell  = [tableView dequeueReusableCellWithIdentifier:@"cellId" forIndexPath:indexPath];
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    [cell.contentView setBackgroundColor:color];
    return cell;
}
@end
